Fit
===

Fit ist das Klausurenarchiv von Spline an der Freien Universität Berlin.
Jegliche Daten werden in einem Git-Repository (vom Typ 'bare') verwaltet.
Klausuren können über ein Webinterface einfach hoch- und heruntergezuladen
werden. Fit ist in Python2 mit Hilfe von Flask und pygit2 geschrieben.


Setup
-------
  $ git clone .../fit.git                  # Source-Code herunterladen
  $ cd fit
  $ cp settings.py.sample settings.py      # Konfigurationsdatei erstellen
  $ nano settings.py
  $ pip install -r requirements.txt        # Abhängikeiten installieren
  $ export LD_LIBRARY_PATH=/usr/local/lib/ # libgit2.so zum LD Path hinzufügen
  $ python app.py                          # Test-Server starten


Config
------
* MAX_CONTENT_LENGTH - Maximale Größe von Dateien beim Upload (in Bytes)
* DEBUG
* FORM_START_YEAR - Startjahr für Upload-Formular
* ALLOWD_EXTENSIONS - Erlaubte Dateien beim Upload
* STUDIES - Dictionary von Studiengängen. Die Liste von Tuples sind Basismodule
            eines Studienganges. Sie werden bei der Modul-Auswahl beim
            Upload-Formular immer angezeigt.


Nginx
-----
Um das Klausurenarchiv hinter einem Nginx laufen zu lassen, sollte dieser
erstmal konfiguriert werden. Hierzu am besten die nginx.cfg benutzen und die
DOMAIN_URL, sowie INSTALL_PATH mit den korrekten Werten ersetzten. Danach kann
das Klausurenarchiv mit Hilfe von uwsgi gestartet werden:

  $ LD_LIBRARY_PATH=/usr/local/lib/ uwsgi -s /tmp/uwsgi.sock -w app:app &
  $ chmod 777 /tmp/uwsgi.sock

Damit die Zip- und Tar-Gz-Archive korrekt erstellt werden, sollten noch Cronjobs
für jeden Studiengang eingerichtet werden:

$ cronjob -e
0 *   *   *   *    cd INSTALL_PATH/static/studies/informatik.git && /usr/bin/git archive -o ../informatik.zip HEAD


Munin
-----
Für das Munin-Plugin sollte unter /etc/munin/plugins folgendes Skript hinterlegt
werden:

  #!/bin/bash
  
  cd INSTALL_PATH
  LD_LIBRARY_PATH=/usr/local/lib/ python munin.py $1
