import sys
from itertools import chain
from app import fit


if __name__ == "__main__":
  entries = {}

  if len(sys.argv) > 1 and sys.argv[1] == 'config':
    print 'graph_title Klausuren'
    print 'graph_vlabel Amount'
    for label in fit.keys():
      print '%s.label %s' % (label, label.capitalize())

  else:
    for label, value in fit.items():
      # fetch all file entries and chain them as a list of tuples
      module_list = chain(*map(value.get_module, value.get_modules()))
      amount = sum([len(x[1]) for x in module_list])
      print '%s.value %d' % (label, amount)
